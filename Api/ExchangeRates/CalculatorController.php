<?php

declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\Calculator\ExchangeRateCalculatorInterface;
use Api\Response;
use Api\ResponseInterface;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class CalculatorController
{
    /**
     * @var ExchangeRateCalculatorInterface
     */
    private $exchangeRateCalculator;

    public function __construct(ExchangeRateCalculatorInterface $exchangeRateCalculator)
    {
        $this->exchangeRateCalculator = $exchangeRateCalculator;
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     *
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
        $amount = (float) ($inputArray['amount'] ?? 1);
        $exchangeRate = $this->exchangeRateCalculator->getExchangeRate(
            $inputArray['fromCurrency'],
            $inputArray['toCurrency'],
            $amount
        );

        return new Response([
            'from' => $exchangeRate->getFromCurrencyCode(),
            'to' => $exchangeRate->getToCurrencyCode(),
            'amount' => $exchangeRate->getAmount(),
        ], SymfonyResponse::HTTP_OK);
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     *
     * @return ResponseInterface
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {
    }

}
