<?php

namespace Api;

class Response implements ResponseInterface
{
    /**
     * @var array
     */
    private $content;
    /**
     * @var int
     */
    private $httpCode;

    public function __construct(array $content, int $code)
    {
        $this->content = $content;
        $this->httpCode = $code;
    }

    public function getContent(bool $jsonEncoded = true)
    {
        return $jsonEncoded ? \json_encode($this->content) : $this->content;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
