<?php

namespace Api\Model;

class ExchangeRate
{
    /**
     * @var string
     */
    private $fromCurrencyCode;
    /**
     * @var string
     */
    private $toCurrencyCode;
    /**
     * @var float
     */
    private $amount;

    public function __construct(string $fromCurrencyCode, string $toCurrencyCode, float $amount)
    {
        $this->fromCurrencyCode = $fromCurrencyCode;
        $this->toCurrencyCode = $toCurrencyCode;
        $this->amount = $amount;
    }

    public function getFromCurrencyCode(): string
    {
        return $this->fromCurrencyCode;
    }

    /**
     * @return string
     */
    public function getToCurrencyCode(): string
    {
        return $this->toCurrencyCode;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }
}
