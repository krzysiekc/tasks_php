<?php

namespace Api\Model;

class PlnExchangeRateTable
{
    /**
     * @var array
     */
    private $exchangeRates;

    public function __construct(array $exchangeRates)
    {
        $this->exchangeRates = $exchangeRates;
    }

    public function getExchangeRate(string $currencyCode): float
    {
        return $this->exchangeRates[$currencyCode];
    }
}
