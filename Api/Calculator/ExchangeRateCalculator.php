<?php

namespace Api\Calculator;

use Api\ApiClient\ExchangeRateApiClientInterface;
use Api\Model\ExchangeRate;

class ExchangeRateCalculator implements ExchangeRateCalculatorInterface
{
    /**
     * @var ExchangeRateApiClientInterface
     */
    private $apiClient;

    public function __construct(ExchangeRateApiClientInterface $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function getExchangeRate(string $from, string $to, float $amount): ExchangeRate
    {
        $table = $this->apiClient->getPlnExchangeRateTable();
        $rate = round(($table->getExchangeRate($from) / $table->getExchangeRate($to)) * $amount, 5);

        return new ExchangeRate($from, $to, $rate);
    }
}
