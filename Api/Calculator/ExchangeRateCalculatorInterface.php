<?php

namespace Api\Calculator;

use Api\Model\ExchangeRate;

interface ExchangeRateCalculatorInterface
{
    public function getExchangeRate(string $from, string $to, float $amount): ExchangeRate;
}
