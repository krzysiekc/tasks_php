<?php

namespace Api\ApiClient;

use Api\Model\PlnExchangeRateTable;

interface ExchangeRateApiClientInterface
{
    public function getPlnExchangeRateTable(): PlnExchangeRateTable;
}
