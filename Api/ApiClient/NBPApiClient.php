<?php

namespace Api\ApiClient;

use Api\Model\PlnExchangeRateTable;
use DateTime;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NBPApiClient implements ExchangeRateApiClientInterface
{
    private const NBP_API_URL_TABLE_A = 'http://api.nbp.pl/api/exchangerates/tables/a';
    /**
     * @var HttpClientInterface
     */
    private $client;
    /**
     * @var CacheInterface
     */
    private $cache;

    public function __construct(HttpClientInterface $client, AdapterInterface $cacheAdapter)
    {
        $this->client = $client;
        $this->cache = $cacheAdapter;
    }

    /**
     * @return PlnExchangeRateTable
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws InvalidArgumentException
     */
    public function getPlnExchangeRateTable(): PlnExchangeRateTable
    {
        $content = $this->cache->get('nbp_exchange_rate_table_a', function (ItemInterface $item) {
            $item->expiresAt(new DateTime('tomorrow 12:15'));

            return $this->client->request(
                'GET',
                self::NBP_API_URL_TABLE_A,
                ['headers' => ['Accept' => 'application/json']]
            )->toArray(true);
        });

        $exchangeRates = [];
        foreach ($content[0]['rates'] as $currency) {
            $exchangeRates[$currency['code']] = (float) $currency['mid'];
        }

        return new PlnExchangeRateTable($exchangeRates);
    }
}
