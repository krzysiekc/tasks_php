<?php

use Api\ApiClient\NBPApiClient;
use Api\Calculator\ExchangeRateCalculator;
use Api\ExchangeRates\CalculatorController;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpClient\HttpClient;

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    4
 * Actual Development Time [in hours]:      2
 * Your thoughts and comments:              I used the filesystem cache but in a real application it would be a more performant storage(for example redis)
 *                                          cache expires when the table in the API is updated, according to the website it's every day at 12:15 - the expiry time should be an injected param but I kept it simple
 *                                          in a real use case we would validate the currency codes against what the table contains but api was down so I skipped it
 *                                          there was nothing in readme about tests so to keep this short I didn't create any
 *
 * ============================================
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */
$_GET = [
    'fromCurrency' => 'USD',
    'toCurrency' => 'GBP',
    'amount' => 50,
];

$controller = new CalculatorController(
    new ExchangeRateCalculator(
        new NBPApiClient(
            HttpClient::create(),
            new FilesystemAdapter('nbp')
        )
    )
);
$response = $controller->calculateCurrentExchangeRateAction($_GET);

// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
